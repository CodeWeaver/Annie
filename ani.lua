--[[
Logan "CodeWeaver" Hall, 2021
]]

local Ani = {}


--[[ Fields ]]
Ani.freeze	= false	-- Animation will freeze when set to true
Ani.maxstep	= 1/30	-- If love.timer.getDelta() is larger than this, use this value instead
Ani.texture	= nil
Ani.fps		= 30
Ani.quad	= nil
Ani._keyframes	= {}
Ani._delta	= 0
Ani._index	= 0

local objlist = {}

local errstr = "Annie: keyframes must be an array of one or more tables containing keyframe data"


--[[ Constructors ]]
-- Ani.newAni(keyframes)
-- Ani.newAni(texture, fps, quads)
function Ani.newAni(a, fps, quads)
	local self = {}
	setmetatable(self, { __index = Ani })

	if type(a) == "table" then
		self._keyframes	= a
	else
		assert(type(quads) == "table" and quads[1]:typeOf("Quad"), "Annie: quads must be an array of Quads")
		self.texture	= a
		self.fps	= fps
		self._keyframes	= quads
	end

	self:step()
	table.insert(objlist, self)
	return self
end


--[[ Methods ]]
function Ani:draw(x, y, r, sx, sy, ox, oy, kx, ky)
	-- Draw current frame
	if self.quad then
		love.graphics.draw(self.texture, self.quad, x, y, r, sx, sy, ox, oy, kx, ky)
	else
		love.graphics.draw(self.texture, x, y, r, sx, sy, ox, oy, kx, ky)
	end
end

function Ani:getDimensions()
	if self.quad then
		local _,_,w,h = self.quad:getViewport()
		return w,h
	else
		return self.texture:getDimensions()
	end
end

function Ani:frame(num)
	self._delta = 0
	self._index = num
	self:step(0)
end

function Ani:step(frames)
	self._index = self._index + (frames or 1)

	if self._index > #self._keyframes then self._index = 1 end	-- If past last keyframe, wrap back to beginning
	local keyframe = self._keyframes[self._index]
	if type(keyframe) == "table" then
		for k,v in pairs(keyframe) do self[k] = v end	-- Apply this keyframe's data to self (for fields that persist across frames)
	else
		self.quad = keyframe
	end
end

function Ani:_stepper(dt)
	-- Step frame if enough time has passed
	if not self.freeze then
		self._delta = self._delta + math.min(dt or love.timer.getDelta(), self.maxstep)
		if type(self.fps) == "number" and self.fps > 0 and self._delta > 1/self.fps then
			if self.test then print(self._delta,self._index) end
			self._delta = self._delta - 1/self.fps
			self:step()
		end
	end
end

function Ani.processAnis(dt)
	for i,ani in ipairs(objlist) do
		ani:_stepper(dt)
	end
end

local love_update
function Ani.enableAutoStep()
	if type(love.update) == "function" then
		love_update = love.update
		love.update = function(dt)
			love_update(dt)
			Ani.processAnis(dt)
		end
	else
		love.update = Ani.processAnis
	end
end

return Ani
